using Images
using Statistics
using Plots
using Interact

# Sigmodial with out weight and bias
function σ(x)
    return 1 /(1 + exp(-x))
end
# Sigmoidal function with Weight & Bias
function σ(x, w, b)
    return 1/(1 + exp(-w * x + b))
end
# Test sigmoidal function
println("plain Sigmodial :", σ(2))
# Test sigmoidal function with weight and bias
println("Sigmodial with weight and bias: ", σ(2, 2, 1))



#Load apple & banana images
apple = load("data/apple.jpg")
banana = load("data/banana.jpg")

#Find mean Red of apple and banana
apple_mean_red = mean(Float64.(red.(apple)))
banana_mean_red = mean(Float64.(red.(banana)))

println("Apple mean red: ", apple_mean_red, " & banana red mean: ", banana_mean_red)


#Find mean Red of apple and banana
apple_mean_green = mean(Float64.(green.(apple)))
banana_mean_green = mean(Float64.(green.(banana)))

println("Apple mean green: ", apple_mean_green, " & banana green mean: ", banana_mean_green)



#Find mean Blue of apple and banana
apple_mean_blue = mean(Float64.(blue.(apple)))
banana_mean_blue = mean(Float64.(blue.(banana)))

println("Apple mean blue:  $apple_mean_blue & banana blue mean: $banana_mean_blue")

#Plot Sigmoidal
gr()

@manipulate for w in -10:0.01:30, b in 0:0.1:20
    plot(x->σ(x, w, b), xlim=(-0, 1), ylim=(-0.1, 1.1), label="model", legend=:topleft, lw =9)
    scatter!([apple_mean_green], [0.0], label="apple", ms=5)
    scatter!([banana_mean_green], [1.0], label="banana", ms=5)
end
