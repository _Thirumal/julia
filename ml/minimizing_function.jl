using Plots
using Interact

#sigmoidal
σ(x) = 1/(1-exp(-x))
#pass weight
f(x, w) = σ(x * w)

x1 =  2
y1 = 0.8

#Loss function
L1(w) = (y1 - f(x1, w))^2

#backgroud
gr()
#plot
plot(L1, -2, 1.5, xlabel = "w", ylabel="L1", leg=false)
