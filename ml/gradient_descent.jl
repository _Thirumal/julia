using Plots;
gr()
using Images;
using Interact

σ(x,w,b) = 1 / (1 + exp(-w*x+b))

#Load apple & banana images
apple = load("data/apple.jpg")
banana = load("data/banana.jpg")

apple_green_amount =  mean(Float64.(green.(apple)))
banana_green_amount = mean(Float64.(green.(banana)));

@manipulate for w in -10:0.01:30, b in 0:0.1:30

    plot(x->σ(x,w,b), 0, 1, label="Model", legend = :topleft, lw=3)
    scatter!([apple_green_amount],  [0.0], label="Apple")
    scatter!([banana_green_amount], [1.0], label="Banana")

end
