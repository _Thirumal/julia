using Plots
using Interact

x1=[1, 2, 3]
y=[2, 4, 6]
plot(x1, y, label="simple plot", lw=3)

g = x -> x * 3
y = g.(x1)
plot!(x1, y, label="function usage", lw=3)

h = x -> x * 4
plot!(x->h(x), x1, label="plot with input", lw=3)

i = x -> x * 5
plot!(x -> i(x), 1:3, label="textbook version", lw=3)
