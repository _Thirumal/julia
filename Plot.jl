using Plots

globaltemperatures = [14.4, 14.5, 14.8, 15.2, 15.5, 15.8]
numpirates = [45000, 20000, 15000, 5000, 400, 17];
plot(numpirates, globaltemperatures, label="line")
scatter!(numpirates, globaltemperatures, label="points")
xlabel!("Number of Pirates [Approximate]")
ylabel!("Global Temperature (C)")
title!("Influence of pirate population on global warming")

xflip!()

unicodeplots();

#Multiple dispatch
using Plots;
gr()
x = pi*(0:0.001:4)
print(x)
println(0:0.001)

plot(x, sin.(x),    c="black", label="Fun")
plot!(x, (12*sin).(x),    c="green", label="Num * Fun")
plot!(x, (sin*12).(x),    c="red", alpha=0.9, label="Fun * Num")
plot!(x, (5*sin*exp).(x), c="blue", alpha=0.2, label="Num * Fun * Fun")


A = rand(1:9,3,4)
print(A)
A\b
print(A\b)


using Plots
x = 1:10; y = rand(10, 2);
println("X: ", x)
println("Y: ", y) # These are the plotting data
plot(x,y)
