function sample(name)
    println("First functon: $name")
end

sample("Thirumal")


function f(x)
    x^2
end

println("f(3): ", f(3))

println(map(f, [1, 2, 3]))

println("BroadCast: ", broadcast(f, [1, 2, 3, 4]))

println("Different way of broadcast: ", f.([1, 2, 3, 4, 5]))

##explicit declare type of parameter

foo(x::String, y::String) = print("My input x is $x and y is $y")

@which foo("Hello", "world")

#foo("I am not going to work", 1, 3)
