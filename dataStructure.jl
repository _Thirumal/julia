#tuples (IMmutable)
myfavoriteanimals = ("penguins", "cats", "sugargliders")
print(myfavoriteanimals)
print(myfavoriteanimals[1])
#myfavoriteanimals[1] = "otters"
# Not possible to change
#print("After changing index: ", myfavoriteanimals[1])


##Dictionaries

phone = Dict("Thirumal" => "8892482627", "Jhon" => "8973697871")

println(phone["Thirumal"])

phone["Jessica"] = "312312"

println(phone)
##Arrays (Mutable)

item = ["Apple", "Orange", "PineApple"]
println(item)
item[1] = "Mango"
println(item)

push!(item, "newItempushed")
println("Item after push: ", item)

pop!(item)

println("After pop: ", item)
