1 + 1
s2 = "hi"
s3 = " welcome"
print(s2 * s3)
σ = 12;
print("Sigma : ", σ)

#using Images
#im = load("aExm7oe_460s.jpg")
sampArray = [1, 2, 3, 4]
print(sampArray)
#
mixArray = [1, 2, 3/2, "Hi", "welcome"]
print(mixArray)
push!(mixArray, "12")

println(mixArray)
pop!(mixArray)
println(mixArray)


copyArray = mixArray;
print(copyArray)
copyArray[1] = 1221;
print(copyArray)

#using plots

a = rand(10^7)
print(a)
#sum(a)
print(@time sum(a))

arr = [1, 2, 3, 4, 4, 5, 6]
for c in arr
    println(c)
end

download("https://upload.wikimedia.org/wikipedia/commons/0/0d/Petaurus_breviceps-Cayley.jpg", "sugar-glider.jpg")
load("sugar-glider.jpg")

using Example
hello("it's me. I was wondering if after all these years you'd like to meet.")

using Colors
palette = distinguishable_colors(100)
print(palette)

a = [1 2 3; 4 5 6]
typeof(a)
size(a)

using Images
animal = load("sugar-glider.jpg")
typeof(animal)
size(animal)
#typeof(animal)
#red(animal)
#size(animal)
#animal[100, 90]
#mean(red.(animal))
dump(typeof(animal[40, 60]))
using Plots
histogram(float.(green.(animal[:])),color="red",label="animal", normalize=true, nbins=25)

# Flux
using Flux
print(σ)

model = Dense(2, 1, σ)
println("Weight: ", model.W)
println("hello")
println("biased: ", model.b)


using Flux
print("relu: ", relu.([-3, 3]))

a = rand(10^7) # 1D vector of random numbers, uniform on [0,1)
@time print(sum(a))
